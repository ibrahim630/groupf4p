#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>
#include <assert.h>

#ifdef WIN32
#include <windows.h>
#elif _POSIX_C_SOURCE >= 199309L
#include <time.h>   // for nanosleep
#else


#endif

int random_generate(int lower, int upper)
{
    // generate random numbers in the range [lower,upper]
    int rand_num = (rand() % (upper - lower + 1)) + lower;
    return rand_num;
}

void sleep_ms(int milliseconds){ // cross-platform sleep function
#ifdef WIN32
    Sleep(milliseconds);
#elif _POSIX_C_SOURCE >= 199309L
    struct timespec ts;
    ts.tv_sec = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;
    nanosleep(&ts, NULL);
#else
    if (milliseconds >= 1000)
        sleep(milliseconds / 1000);
    usleep((milliseconds % 1000) * 1000);
#endif
}


/** Multithreaded Readers-Writers Problem
 *  Here we have a file to which any number of reader threads may have simultaneous access,
 *  as long as they are just reading. But if a thread is to write to the file, it must have
 *  exclusive access.
 *
 *  So, multiple reader threads can read from the file simultaneously but only one writer thread
 *  can write to the file at a single time.
 *
 *  Here we solve the readers-writers problem using Monitor
 *  Monitor: A monitor is a combination of locks and zero or more condition variable.
 *  Condition Variables: A queue of threads waiting for something inside a critical section.
 *  Operations on Condition Variables:
 *    - Wait (& lock): Atomically release lock and go to sleep. Re-acquire lock later, before returning.
 *    - Signal (): Wake up one waiter, if any
 *    - Broadcast (): Wake up all waiters
 *
 *  Condition to Stop the program: Press control + c to stop the program.
 *
 *  Questions:
 *
 *  Can the Readers starve? Consider reader entry code.
 *
 *    // As long as there is at least one writer
        while (!(writers == 0))
        {
            // Wait for the condition variable to be signalled
            // Note: This call releases the mutex lock when called and establishes the mutex lock again before returning
            pthread_cond_wait(&readerQueue, &mutex);
            // Why sleep on the condition variable here?
            // We want the writer to go first, give priority to writers
        }
 *
 *  Yes, We go to sleep and when we wake up we check the condition again.
 *  If the writers keep coming along during that time then it will prevent us to go forward and the readers will starve.
 *
 *  What if we erase the condition check in reader exit? Does this still work?
 *  In Monitor we always check our condition when we wake up.
 *
 *  What if we turn the signal into broadcast?
 *  We will wake up all the waiting writers but only one can have access to the critical section.
 *  And the rest will go back to sleep.
 */

// Declare the mutex and condition variables
// We use two condition variables readerQue and writerQueue, to represent readerCount and writerCount
// waiting for notification that their respective guards are true.
pthread_mutex_t mutex;
pthread_cond_t readerQueue;
pthread_cond_t writerQueue;

// State variables protected by a lock called mutex
int readerCount = 0; // number of active readers
int writerCount = 0; // number of active writers

// Data that will be written to the file by the writer thread
int fileData = 0;

// Pointer to a file
FILE *fp = NULL;

// Loop control variable
bool quit = false;

void reader(void *threadNumber)
{
    // check into the system
    // Run the reader loop
    while (!quit)
    {
        // Wait for random amount of time [0 to 1000] ms
        sleep_ms(random_generate(1, 1000));
        pthread_mutex_lock(&mutex);

        // As long as there is at least one writer
        while (!(writerCount == 0))
        {
            // Wait for the condition variable to be signalled
            // Note: This call releases the mutex lock when called and establishes the mutex lock again before returning
            pthread_cond_wait(&readerQueue, &mutex);
            // Why sleep on the condition variable here?
            // We want the writer to go first, give priority to writers
        }

        // Any number of reader threads
        readerCount++;
        // Now unlock the mutex
        pthread_mutex_unlock(&mutex);

        // Why are we releasing the lock here before we access the file?
        // We have to release the lock here so that other readers can come through this entry point.
        // So that multiple readers can access the file.
        // Allow more readers

        // Why can't we broadcast all the waiting writers here?
        // Because we only want one writer running at a time.
        // We want to signal one writer at a time.

        // Before reading the file there will be zero writer thread and any number of reader thread
        assert((writerCount == 0) && (readerCount > 0));

        // read only access to the file
        // Assume the readers take a while to read the database/file.
        printf("\n\nStart Reading: File is read by reader thread %d", *(int *)threadNumber);
        int data;
        rewind(fp);
        fscanf(fp, "%d", &data); // Read the data from the file
        printf("\nFile content: %d", data);
        printf("\nEnd Reading: File is read by reader thread %d", *(int *)threadNumber);

        // Grab the lock before changing any state variables
        pthread_mutex_lock(&mutex);
        // If there is no reader to read from the file
        // We want to signal one writer at a time.
        readerCount--;
        if (readerCount == 0)
        {
            // Signal the writer thread waiting in the writer queue
            // Wake up one waiting writer, if any
            pthread_cond_signal(&writerQueue);
        }
        pthread_mutex_unlock(&mutex);
    }
}

void writer(void *threadNumber)
{
    // check into the system
    // Run the writer loop
    while (!quit)
    {
        // Wait for random amount of time [0 to 10000] ms
        sleep_ms(random_generate(1, 1000));

        // Lock the mutex to make sure that the writer thread has exclusive access to the file
        pthread_mutex_lock(&mutex);

        // As long as there are other reader thread/s or other writer thread/s
        while (!((readerCount == 0) && (writerCount == 0)))
        {
            // Wait for the condition variable to be signalled
            // Note: This call unlock the mutex when called and relocks it before returning
            pthread_cond_wait(&writerQueue, &mutex);
        }

        writerCount++; // only one writer at a time

        // Before writing to the file there will be one writer thread and zero reader thread
        assert((readerCount == 0) && (writerCount == 1));

        // Read/Write access to the file
        printf("\n\nStart Writing: File is written by writer thread %d", *(int *)threadNumber);
        fileData++; // Increment the file data by 1
        rewind(fp);
        fprintf(fp, "%d", fileData);
        printf("\nFile content: %d", fileData); // Write the fileData to the file
        printf("\nEnd Writing: File is written by writer thread %d", *(int *)threadNumber);

        writerCount--; // only one writer at a time

        // Signal the condition variable that data is written to the file
        // Give priority to the writers
        // Wake up one writer
        pthread_cond_signal(&writerQueue);

        // Why give priority to writers?
        // 1. We have choosen to give priority to writers
        // 2. There are more readers than writers, so we just want to get them out of the way
        // 3. Writers typically update the database and readers basically want the most recent data

        // What happens if there is signal and nobody waiting?
        // The key thing with Monitor is if we signal and there is nobody waiting, then nothing happens

        // Wake up reader
        // We use pthread_cond_broadcast to insure that all the currently waiting readers are released
        // Wake up all the waiting readers
        pthread_cond_broadcast(&readerQueue);

        // Why broadcast here instead of signal?
        // We will have multiple readers. Signal is used to broadcast one waiter at a time.
        // On the other hand, broadcast is used to wake up all the waiters.

        // Done, ulock the mutex
        pthread_mutex_unlock(&mutex);
    }
}

int main(int argc, const char **argv)
{
    int sleep_time, reader_count_input, writer_count_input;

    printf("How long (second) to sleep before terminating: ");
    scanf("%d", &sleep_time);

    printf("The number of reader threads: ");
    scanf("%d", &reader_count_input);

    printf("The number of writer threads: ");
    scanf("%d", &writer_count_input);

    // thread identifiers
    // declare thread ids, an array of threads to be joined upon
    pthread_t id_reader[reader_count_input], id_writer[writer_count_input];

    // initialize the mutex and condition variables
    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&readerQueue, NULL);
    pthread_cond_init(&writerQueue, NULL);

    // Declare variables to store the number of the threads
    int reader_numbers[reader_count_input];
    int writer_numbers[writer_count_input];

    // Insert values into the array, each value represents a thread number
    for (int i = 0; i < reader_count_input; i++)
    {
        reader_numbers[i] = (i + 1);
        if (i >= writer_count_input)
        {
            continue;
        }

        writer_numbers[i] = i + 1;
    }

    // open the file
    if ((fp = fopen("counter.txt", "w+")) == NULL)
    {
        printf("\nError opening file!\n");
        return 1;
    }

    // Create threads
    for (int i = 0; i < reader_count_input; i++)
    {
        pthread_create(&id_reader[i], NULL, (void *)reader, &reader_numbers[i]); // create reader threads
        if (i >= writer_count_input)
        {
            continue;
        }
        pthread_create(&id_writer[i], NULL, (void *)writer, &writer_numbers[i]); // create writer threads
    }

    // wait for certain amount of time before terminating
    sleep(sleep_time);

    for (size_t i = 0; i < reader_count_input; ++i)
    {
        pthread_cancel(id_reader[i]);
    }
    for (size_t i = 0; i < writer_count_input; ++i)
    {
        pthread_cancel(id_writer[i]);
    }

    // when all the threads are done, the mutex and the condition variables are destroyed.
    pthread_cond_destroy(&readerQueue);
    pthread_cond_destroy(&writerQueue);
    pthread_mutex_destroy(&mutex);

    fprintf(fp, "\nDone!\n");
    fclose(fp);
    return 0;
}
