#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>

#ifdef WIN32
#include <windows.h>
#elif _POSIX_C_SOURCE >= 199309L
#include <time.h>   // for nanosleep
#else


#endif

int random_generate(int lower, int upper)
{
    // generate random numbers in the range [lower,upper]
    int rand_num = (rand() % (upper - lower + 1)) + lower;
    return rand_num;
}

void sleep_ms(int milliseconds){ // cross-platform sleep function
#ifdef WIN32
    Sleep(milliseconds);
#elif _POSIX_C_SOURCE >= 199309L
    struct timespec ts;
    ts.tv_sec = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;
    nanosleep(&ts, NULL);
#else
    if (milliseconds >= 1000)
        sleep(milliseconds / 1000);
    usleep((milliseconds % 1000) * 1000);
#endif
}


#define DEBUG true
#define debug_print(fmt, ...) do { if (DEBUG) fprintf(stdout, fmt, __VA_ARGS__); } while (0)

//rw_mutex is semaphore but being used like temp_mutex
//for blocking the prevent to writer to get into
// critical resource if any writer or reader already in
sem_t rw_mutex;
//temp_mutex is a temporary mutex not for securing the critical resource
//it is for securing reader_count increment and rw_mutex
pthread_mutex_t temp_mutex;
//empty mutex is for preventing reader read empty file earlier than writer
pthread_mutex_t empty = PTHREAD_MUTEX_INITIALIZER;

//reader_count is for following up how many reader thread inside of critical section
//No need to make it atomic integer since it is already secured with temp_mutex
int reader_count = 0;
//fp is just a file pointer which hold file location and cursor etc...
FILE *fp = NULL;
//start_time is for debug the code and watch if timing works well
time_t start_time;
//sleep_time is an input from user
int sleep_time;
//counter is the variable the code injecting into the file.
//We are following the results by increasing it by comparing reader and writer
int counter = 0;

bool cont = true;

//*data is the logical thread number not a PID
//it is for watching which thread got into and which got out
void *writer(void *data)
{
    while (cont){
        sleep_ms(random_generate(1, 1000));
        //sem_wait decrease the inner value of semaphore
        //if it is not positive already block itself
        // if it is positive, block to nextcomers
        sem_wait(&rw_mutex);
        debug_print("Writer in %d\n",*(int *) data);
        sleep_ms(random_generate(1, 10000));
        //reset file cursor to begin like fseek
        rewind(fp);
        counter++;
        fprintf(fp, "%d", counter);
        printf("File is written by writer thread %d and file content: %d\n", *(int *) data, counter);
        //
        pthread_mutex_unlock(&empty);
        sem_post(&rw_mutex);
    }
    //We don't expect the code(program counter) come here
    //since thread cancel terminate
    //it is left from previous way which we used to use(condition in while)
    debug_print("Writer end \n","");
    return NULL;
}


//*data is the logical thread number not a PID
//it is for watching which thread got into and which got out
void *reader(void *data)
{
    while (cont){
        debug_print("Reader in %d\n", *(int *) data);
        sleep_ms(random_generate(1, 1000));
        // Reader acquire the lock before modifying reader_count
        pthread_mutex_lock(&empty);
        pthread_mutex_lock(&temp_mutex);
        reader_count++;
        if (reader_count == 1) {
            sem_wait(&rw_mutex); // If this id the first reader, then it will block the writer
        }
        pthread_mutex_unlock(&temp_mutex);
        debug_print("Reader unlocked %d\n", *(int *) data);
        sleep_ms(random_generate(1, 10000));

        int count;
        rewind(fp);
        fscanf(fp, "%d", &count);
        printf("File is read by reader thread %d and file content: %d\n", *(int *) data, count);

        debug_print("Reader count %d\n", reader_count);
        // Reader acquire the lock before modifying reader_count
        pthread_mutex_lock(&temp_mutex);
        reader_count--;
        if (reader_count == 0) {
            debug_print("Reader unlock rw_mutex %d \n", reader_count);
            sem_post(&rw_mutex); // If this is the last reader, it will wake up the writer.
        }
        pthread_mutex_unlock(&temp_mutex);
    }
    debug_print("Reader end \n","");
    return NULL;
}

int main(int argc, const char **argv)
{

    int reader_count_input, writer_count_input;

    printf("How long (second) to sleep before terminating: ");
    scanf("%d", &sleep_time);

    printf("The number of reader threads: ");
    scanf("%d", &reader_count_input);

    printf("The number of writer threads: ");
    scanf("%d", &writer_count_input);

    start_time = time(0);

    char buff[100];
    strftime (buff, 100, "%Y-%m-%d %H:%M:%S.000", localtime (&start_time));
    printf ("Started at: %s\n", buff);

    pthread_t writer_thread_ids[writer_count_input],read_thread_ids[reader_count_input];

    if (pthread_mutex_init(&temp_mutex, NULL) != 0) {
        printf("\n temp_mutex init has failed\n");
        return 1;
    }
    if (pthread_mutex_init(&empty, 0) != 0) {
        printf("\n temp_mutex init has failed\n");
        return 1;
    }
    pthread_mutex_lock(&empty);
    sem_init(&rw_mutex,0,1);

    time_t t;
    /* Intializes random number generator */
    srand((unsigned) time(&t));

    int reader_numbers[reader_count_input];
    int writer_numbers[writer_count_input];

    // Insert values into the array, each value represents a thread number
    for (int i = 0; i < reader_count_input; i++)
    {
        reader_numbers[i] = (i + 1);
        if (i >= writer_count_input)
        {
            continue;
        }

        writer_numbers[i] = i + 1;
    }

    if ((fp = fopen("output_semaphore.txt", "w+")) == NULL)
    {
        printf("\nError opening file!\n");
        return 1;
    }

    for(int i = 0; i < reader_count_input; i++) {
        pthread_create(&read_thread_ids[i], NULL, (void *) reader, &reader_numbers[i]); // create reader threads
    }
    for(int i = 0; i < writer_count_input; i++) {
        pthread_create(&writer_thread_ids[i], NULL, (void *)writer, &writer_numbers[i]); // create reader threads
    }

    sleep(sleep_time);
    for(size_t i = 0 ; i < reader_count_input; ++i){
        pthread_cancel(read_thread_ids[i]);
    }
    for(size_t i = 0 ; i < writer_count_input; ++i){
        pthread_cancel(writer_thread_ids[i]);
    }

    time_t end_time= time(0);

    strftime (buff, 100, "%Y-%m-%d %H:%M:%S.000", localtime (&end_time));
    debug_print("Ended at: %s\n", buff);

    printf("END");

    pthread_mutex_destroy(&temp_mutex);
    sem_destroy(&rw_mutex);
    fclose(fp);

    return 0;

}